---
title: Talk schedule now available!
date: 2018-08-29 11:30:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham 2018 [schedule](/talks/schedule/) is now available. It features three
tracks spread across October 5th and 6th.

<!--more-->

This year's program features over 60 speakers giving 60 talks covering a wide
array of topics, from application design to data science to natural language
processing to web development. There will also be three keynote talks and
lightning talks.

Thanks to everyone who submitted a talk, voted on talks, or signed up for the
program committee.
