---
title: Registration Information
---

{% if site.data.event.registration_closed %}
  Registration for {{ site.data.event.name }} is no longer open.
{% elsif site.data.event.registration_url %}
  To register for {{ site.data.event.name }}, visit the
  [registration page]({{ site.data.event.registration_url }}).
{% else %}
  Registration for {{ site.data.event.name }} isn't open yet.
{% endif %}

## Prices
The following ticket options are available. Select the one that is most
appropriate for your type of registration.

| Registration | Early Bird Rate | Regular Rate |
| ------------ |:---------------:|:------------:|
| Corporate    |                 | $300         |
| Individual   | $100            | $200         |
| Academic     |                 | $75          |
{: .table.table-striped.table-bordered }

## Which Ticket Should I Buy?
Corporate
: If your company is paying for you to attend, we request that you register at
the corporate rate. This will help keep the costs down for everyone else,
especially students and those in need of financial assistance.
: If you'd like to use a purchase order to buy tickets, send an email to
[corptickets@pygotham.org](mailto:corptickets@pygotham.org) with your company's
name and address and the number of tickets you'd like to buy. We'll send you an
invoice and a registration code that you can use to claim your tickets.

Individual
: If you're paying for the conference yourself, this is the right registration
type for you. Employees of non-profits can also opt to register at the
individual rate.

Academic
: Students, faculty, and staff of academic institutions are allowed to purchase
tickets at a discounted rate. Please be prepared to provide appropriate
identification at the registration desk.

## Early Bird Rate
Only a limited number of individual rate tickets are available at this rate.
Once they're sold out, no more will be made available.

Please note: due to the limited number of these tickets available, they are
reserved for individuals paying for the conference themselves. Corporate
attendees who do not qualify for the individual rate must still purchase a
corporate ticket.

## Financial Aid
We aim for {{ site.data.event.name }} to be open to all to attend, and we strive
to make the conference as accessible as possible. To request financial aid,
please fill out the {% if site.data.event.financial_aid_url %}
[application form]({{ site.data.event.financial_aid_url }}){% else %}
application form (coming soon){% endif %}. Feel free to ping us at
[financialaid@pygotham.org](mailto:financialaid@pygotham.org) if you have any
questions.

{% if site.data.event.financial_aid_deadline %}
We will begin reviewing financial aid applications on 
{{ site.data.event.financial_aid_deadline | date: '%A, %B %-d, %Y' }}. All
applications received before then will be reviewed first. If there is still aid
available after they've all been processed, any additional applications will be
reviewed on a first-come, first-served basis until all of the available aid has
been awarded.
{% endif %}

## Scholarships
Thanks to our sponsors, we're able to offer a number of diversity scholarships
to individuals who are part of underrepresented groups. These scholarships are
distributed through partnerships with local community groups who award them to
their members. If you are interested in learning more or represent a group that
would like to help find scholarship recipients, please send an email to
[scholarships@pygotham.org](mailto:scholarships@pygotham.org).

[Read more about diversity at {{ site.data.event.name }}]({% link about/diversity.md %})

## What's Included?
Along with your conference registration, attendees will receive breakfast,
lunch, and break refreshments.

## Refund Policy
Refunds requested more than 48 hours prior to the start of the event will be
processed, resulting in a return of the full purchase price of the ticket.

We apologize that within 48 hours of the event, catering and capacity numbers
will be locked in. We will not be able to provide a refund at that time.

{% if site.data.event.registration_url %}
  In the event of cancellation, you may request a refund of your registration by
  visiting the [registration page]({{ site.data.event.registration_url }}).
{% endif %}
