---
name: Michael V. Battista
talks:
- Creating a DVR with Python
---

Michael V. Battista has been developing professionally for seven years. Starting from a Perl and PHP background and graduating from Rensselaer Polytechnic Institute in 2008, Michael has been developing Python professionally since 2015. In his professional career, Michael has worked with companies such as [Percolate](http://www.percolate.com), [Karhoo](http://karhoo.com), and [MediaMath](http://mediamath.com).

In his spare time, Michael also works with [WDWNT](http://www.wdwnt.com) as an iOS developer and technical support. Michael also volunteers with [San Diego Comic Con](https://www.comic-con.org/) and [PAX conventions](http://www.paxsite.com/).