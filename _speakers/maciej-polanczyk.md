---
name: "Maciej Pola\u0144czyk"
talks:
- Vodka powered by Whiskeyberry PI
---

I started my programming journey with Basic and C++. I spent few years with Java and finally I discovered Python. I’m keen on having good quality tests. For few years I was working for TomTom in the team responsible for CI and this was a good lesson of how unit tests should be written to provide best information in case of failure. Currently, I work for STX Next where I am developing web services and sharing knowledge about best unit testing practices.