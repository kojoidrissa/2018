---
name: Gregory M. Kapfhammer
talks:
- Using Python, Travis CI, and GitHub to Effectively Teach Programming
---

As an Associate Professor of Computer Science at Allegheny College, Gregory M. Kapfhammer enables students to understand and create innovative software. By partnering with well-known technology companies and both creating and adopting open-source software, Kapfhammer designs and delivers interdisciplinary courses. 

Emphasizing the importance of oral and written communication and teamwork, Gregory Kapfhammer teaches undergraduate courses in areas such as data management, software engineering, and web development, releasing his course materials through GitHub repositories and mobile-ready web sites. An award winning researcher, he involves students in projects leading to the publication of papers and the release of software tools and data sets. 

Gregory Kapfhammer sees the success of his students as his greatest achievement. They have secured positions at top technology companies such as Amazon, Dell, Facebook, Google, Intel, Microsoft, Tumblr, and numerous startups. Many of Kapfhammer's students have also prospered in academia, receiving prestigious research fellowships and paper awards. 

Interested in learning more about Gregory Kapfhammer? You can visit his [web site](https://www.gregorykapfhammer.com/) to access his teaching and research materials and read his blog or you can check out his [GitHub profile](https://github.com/gkapfham) to use his open-source software!